import mlflow
import secret as sc

# Tracking URI could be as well local or distant
# Uncomment the following line to make it distant
# mlflow.set_tracking_uri(uri='https://user-notou-mlflow.user.lab.sspcloud.fr')


params = {"iterations": 10, "param_1": 1, "param_2": 0}

mlflow.set_experiment("Demo 2: Logging")

# Possibility to log the system metrics (CPU usage, RAM usage, ...)
log_system_metrics = False

with mlflow.start_run(log_system_metrics=log_system_metrics):
    # We log the parameters of the experiment
    mlflow.log_params(params=params)

    for i in range(params["iterations"]):
        # We pass that value into the secret model we want to evaluate
        metric_1, metric_2 = sc.unknown_model(
            param_1=params["param_1"], param_2=params["param_2"], iteration=i
        )

        # We log the input and output values to that model
        mlflow.log_metric(
            key="metric_1", value=metric_1, step=i, synchronous=False
        )
        mlflow.log_metric(key="metric_2", value=metric_2, step=i, synchronous=False)

    sc.reset_model()
    mlflow.log_metric(key="final", value=metric_2)

print(f"Simulation Done, final results: {metric_1=} {metric_2=}")
