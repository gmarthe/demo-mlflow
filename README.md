# Demo MLFLow

### Prerequisites

Python 3.8 or later is required.

### Install Dependencies

Two options: 
- Use conda, and create a new environment:
```conda create --name mlflow_demo --file requirements.txt -c conda-forge```

- Use an existing python installation (and/or existing conda env or Venv):
```pip install -r requirements.txt```

## Getting started

This demo revolves around studying the behaviour of a "secret" function that is hosted in the `secret.py` file.
**To avoid spoilers, please don't go looking inside that file**

The demo will process by running, describing and playing with each example_ file in order, introducing new features at each step.
Half of the time will be spent looking and running these scripts, and the other half will be looking at the logged information through the MLFlow user interface.

### Launch MLflow UI

To launch the user interface to browse locally logged data, launch the following command in the same folder as where data was logged:

```mlflow ui```

## Authors

Cyrille MORIN and Maxime VAILLANT

