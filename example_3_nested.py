import mlflow
import secret as sc

# Tracking URI could be as well local or distant
# Uncomment the following line to make it distant
# mlflow.set_tracking_uri(uri='https://user-notou-mlflow.user.lab.sspcloud.fr')

mlflow.set_experiment("Demo 3: Nested Logging")

# Possibility to log the system metrics (CPU usage, RAM usage, ...)
log_system_metrics = False

iterations_range = [10, 50, 100, 150, 300, 500]

with mlflow.start_run(log_system_metrics=log_system_metrics):
    for iteration in iterations_range:

        params = {"iterations": iteration, "param_1": 1, "param_2": 0}

        with mlflow.start_run(log_system_metrics=log_system_metrics, nested=True):
            # We log the parameters of the experiment
            mlflow.log_params(params=params)

            for i in range(params["iterations"]):
                # We pass that value into the secret model we want to evaluate
                metric_1, metric_2 = sc.unknown_model(
                    param_1=params["param_1"], param_2=params["param_2"], iteration=i
                )

                # We log the input and output values to that model
                mlflow.log_metric(
                    key="metric_1", value=metric_1, step=i, synchronous=False
                )
                mlflow.log_metric(key="metric_2", value=metric_2, step=i, synchronous=False)

            sc.reset_model()
            mlflow.log_metric(key="final", value=metric_2)

print("Done!")
