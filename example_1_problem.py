import secret as sc


params = {"iterations": 10, "param_1": 1, "param_2": 0}


for i in range(params["iterations"]):
    # We pass that value into the secret model we want to evaluate
    metric_1, metric_2 = sc.unknown_model(
        param_1=params["param_1"], param_2=params["param_2"], iteration=i
    )

sc.reset_model()
print(f"Simulation Done, final results: {metric_1=} {metric_2=}")
