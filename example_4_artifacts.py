import mlflow
import numpy as np
import matplotlib.pyplot as plt
from plotly import graph_objects as go
import secret as sc

# Tracking URI could be as well local or distant
# Uncomment the following line to make it distant
# mlflow.set_tracking_uri(uri='https://user-notou-mlflow.user.lab.sspcloud.fr')

mlflow.set_experiment("Demo 4: Artifacts Logging")

# Possibility to log the system metrics (CPU usage, RAM usage, ...)
log_system_metrics = False

iterations_range = [10, 50, 100, 150, 300, 500]

with mlflow.start_run(log_system_metrics=log_system_metrics):
    for iteration in iterations_range:

        params = {"iterations": iteration, "param_1": 1, "param_2": 0}
        metric_1_array = np.zeros(params["iterations"])

        with mlflow.start_run(log_system_metrics=log_system_metrics, nested=True):
            # We log the parameters of the experiment
            mlflow.log_params(params=params)

            for i in range(params["iterations"]):
                # We pass that value into the secret model we want to evaluate
                metric_1, metric_2 = sc.unknown_model(
                    param_1=params["param_1"], param_2=params["param_2"], iteration=i
                )
                        
                # We save the metric_1 value for later plotting
                metric_1_array[i] = metric_1

                # We log the input and output values to that model
                mlflow.log_metric(
                    key="metric_1", value=metric_1, step=i, synchronous=False
                )
                mlflow.log_metric(key="metric_2", value=metric_2, step=i, synchronous=False)

            # Save plots as artefacts, first with matplotlib, then plotly
            fig = plt.figure()
            plt.hist(metric_1_array)
            mlflow.log_figure(fig, artifact_file="metric_1_histo.png")

            fig = go.Figure(go.Histogram(x=metric_1_array))
            mlflow.log_figure(fig, artifact_file="metric_1_histo_plotly.html")

            sc.reset_model()
            mlflow.log_metric(key="final", value=metric_2)

print("Done!")
