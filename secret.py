"""
This module contains some secret functions, shush!
"""
import numpy as np
from typing import Tuple

global total
total = 0.


def unknown_model(iteration: int, param_1: float = 0, param_2: float = 1) -> Tuple[float, float]:
    """
    Secret "unknown" function that converts some parameters and an iteration to a pair of metrics

    Parameters
    ----------
    iteration : int
        _description_
    param_1 : float, optional
        First unknown parameter
    param_2 : float, optional
        Second unknown parameter

    Returns
    -------
    float
        returned metric 1
    float
        returned metric 2
    """
    global total

    drawn = np.random.normal(loc=param_2, scale=param_1)
    total += drawn * (200 - iteration) / 1000
    return drawn, total


def reset_model() -> None:
    global total
    total = 0.
